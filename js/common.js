$(function() {
  loader();

  var myVideo = document.getElementById("myVideo");
  var myVideo2 = document.getElementById("myVideo2");
  var myVideo3 = document.getElementById("myVideo3");
  myVideo.play();
  myVideo2.play();
  myVideo3.play();

  $(".navicon-button").click(function() {
    $(this).toggleClass("open");
  });

  let perticles = $(".preloader__textParticle");
  $(".preloader__svg").fadeIn(600);

  setTimeout(function() {
    $(".preloader__textParticle").addClass("preloader__textParticle_white");
  }, 1500);

  function loader(_success) {
    var inner = $(".preloader__timer");

    var w = 0,
      t = setInterval(function() {
        w = w + 1;
        inner.html(w + "%");
        if (w === 100) {
          clearInterval(t);
          w = 0;
          inner.html("Say hello!");
          $("#closePreloader").addClass("preloader__btn_visible");
        }
      }, 25);
  }

  $("#closePreloader").mouseover(function() {
    $(this).html("Hello");
  });

  $("#closePreloader").mouseout(function() {
    $(this).html("You are welcome");
  });

  $("#closePreloader").click(function() {
    AOS.init();

    $("body").css("overflow-y", "auto");
    $(document).scrollTop(0);
    $("#preloader").fadeOut();
  });

  jQuery.each(jQuery("textarea[data-autoresize]"), function() {
    var offset = this.offsetHeight - this.clientHeight;

    var resizeTextarea = function(el) {
      jQuery(el)
        .css("height", "auto")
        .css("height", el.scrollHeight + offset);
    };
    jQuery(this)
      .on("keyup input", function() {
        resizeTextarea(this);
      })
      .removeAttr("data-autoresize");
  });

  $(".header__navList").on("click", "li a", function(event) {
    event.preventDefault();
    $("body").css("overflow-y", "hidden");

    if ($(".menu__btn:checked")) {
      $(".menu__btn").prop("checked", false);
    }

    var id = $(this).attr("href"),
      top = $(id).offset().top - 70;
    $(".header__nav").removeClass("header__nav_show");
    $("body,html").animate({ scrollTop: top }, 1500, function() {
      $("body").css("overflow-y", "auto");
    });
  });

  $(".banner__buttonsWrap").on("click", "a", function(event) {
    event.preventDefault();
    $("body").css("overflow-y", "hidden");
    var id = $(this).attr("href"),
      top = $(id).offset().top;
    $("body,html").animate({ scrollTop: top }, 1500, function() {
      $("body").css("overflow-y", "auto");
    });
  });

  // $(".banner").bind("mousewheel", function(event) {
  //   if (event.originalEvent.wheelDelta / 120 < 0) {
  //     event.preventDefault();
  //     $("body").css("overflow-y", "hidden");
  //     $("body,html").animate(
  //       { scrollTop: $("#services").offset().top },
  //       500,
  //       function() {
  //         $("body").css("overflow-y", "auto");
  //       }
  //     );
  //   }
  // });

  $("form").submit(function() {
    var formID = $(this).attr("id");
    var formNm = $("#" + formID);

    var isValid =
      (
        $("#email")
          .val()
          .match(/.+?\@.+/g) || []
      ).length === 1;

    if (isValid) {
      $("#contactForm").submit(function() {
        var th = $(this);
        $.ajax({
          type: "POST",
          url: "mail.php",
          data: th.serialize()
        }).done(function() {
          $(".successMessage").fadeIn();
          setTimeout(function() {
            th.trigger("reset");
          }, 1000);
        });
        return false;
      });
    }

    return false;
  });

  $(".successMessage__button").click(function() {
    $(".successMessage").fadeOut();
  });

  $(".menu__btn").click(function() {
    if ($(".header__nav").hasClass("header__nav_show")) {
      $(".header").removeClass("header_withBg");
      $("body").css("overflow-y", "auto");
    } else {
      $(".header").addClass("header_withBg");
      $("body").css("overflow-y", "hidden");
    }
    $(".header__nav").toggleClass("header__nav_show");
  });

  $(window).on("scroll", function() {
    var offset = $(window).scrollTop();
    var titleOffset = $(".banner__title").offset().top;

    if (offset > titleOffset) {
      $(".header").addClass("header_withBg");
    } else {
      $(".header").removeClass("header_withBg");
    }
  });
});
